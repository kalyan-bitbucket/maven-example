# Deploy to Artifactory

An [example script](https://bitbucket.org/shah_jainish/project-examples/src/master/deploy-to-artifactory.bash),
and [Maven POM](https://bitbucket.org/shah_jainish/project-examples/src/master/artifactory-maven-plugin-example/pom.xml)
for deploying from Bitbucket Pipelines to an instance of Artifactory.

## How to use it

This example uses the Maven Artifactory Plugin for artifacts and build-info deployment to Artifactory:

* Add required environment variables to your Bitbucket enviroment variables.
* Edit the `pom.xml` file in your repository to add the `artifactory-maven-plugin`. If you copy/paste from this example, make sure to set the value of the *contextUrl* with your Artifactory URL, as well as the other Artifactory properties. For more configuration information see the [Maven Artifactory Plugin documentation](https://www.jfrog.com/confluence/display/RTF/Maven+Artifactory+Plugin).
* Copy `deploy-to-artifactory.bash` to your project.
* Add `deploy-to-artifactory.bash` to your build configuration.


## Required environment variables

* **`ARTIFACTORY_USERNAME`**: (Required) Username for Artifactory instance configured in pom.xml. Example: "jainishs"
* **`ARTIFACTORY_PASSWORD`**: (Required) Password for Artifactory instance configured in pom.xml.
* **`ARTIFACTORY_CONTEXT_URL`**: (Required) URL for Artifactory
* **`buildNumber`**: (Optional) User-visible identification of this build. Example: "12345"


## Deploy to Artifactory

```
if [ -z "$buildNumber" ]; then
	buildNumber=`date +%s`
fi
mvn install -DskipTests=true
mvn test
mvn deploy -Dusername=${ARTIFACTORY_USERNAME} -Dpassword=${ARTIFACTORY_PASSWORD} -Durl=${ARTIFACTORY_CONTEXT_URL}
```

* Uses `-ex` switches (not shown in snippet above) to exit on first error and to echo command before execution. These help with debugging.
* Sets a default `buildNumber`, if one is not otherwise provided.
* Uses Maven to install, test, and deploy the jar passing in environment variables.

## Build configuration

```
image: maven:3.3-jdk-8
pipeline:
    - job:
        script:
            - cd artifactory-maven-plugin-example
            - pwd
            - deploy-to-artifactory.bash
```

* Uses the Docker official Maven image, which already contains Java and Maven.
* Builds and deploys the JAR file to Artifactory.


## Maven plugin documentation

The full plugin documentation is available
[here](https://www.jfrog.com/confluence/display/RTF/Maven+Artifactory+Plugin).
